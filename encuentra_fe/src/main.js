import { createApp } from 'vue'
import inicio from './App.vue'
import router from './router'

createApp(inicio).use(router).mount('#app')
