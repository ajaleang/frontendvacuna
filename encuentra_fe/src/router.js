import { createRouter, createWebHashHistory } from 'vue-router'
import inicio from './App.vue'

import logIn from './components/logIn.vue'


const routes = [
  {
    path: '/',
    name: 'root',
    component: inicio
  },

  {
    path: '/user/logIn',
    name: 'logIn',
    component: logIn
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
